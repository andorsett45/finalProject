package com.oreillyauto.domain;

public class OrlyTable {
    
    //Variables
    private String id;
    private String joke;
    
    
    //Constructor
    public OrlyTable() {
        super();
    }
    
    //Getters & Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }
    public void setJoke(String joke) {
        this.joke = joke;
    }

    //ToString
    /*@Override
    public String toString() {
        return "OrlyTable [id=" + id + ", joke=" + joke + "]";
    }*/

}