package com.oreillyauto.domain;

public class Person {

    //Variables
    private String id;
    private String message;
    
    //Constructors
    public Person() {
        super();
    }  
    
    public Person(String id, String message) {
        super();
        this.id = id;
        this.message = message;
    }

    //Getters & Setters
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    //ToString
    @Override
    public String toString() {
        return "Person [id=" + id + ", message=" + message + "]";
    }
    
}
