package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "EVENT_PROPERTIES")
public class EventProperty implements Serializable{

    private static final long serialVersionUID = -2060173648972096986L;
    
    //Constructors
    public EventProperty() {
        super();
    }
    
    //Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROPERTY_ID", columnDefinition = "INTEGER")
    private Integer propertyId;
    
    /*@Column(name = "EVENT_ID", columnDefinition = "INTEGER")
    private Integer eventId;*/
    
    @Column(name = "EVENT_KEY", columnDefinition = "VARCHAR(50)")
    private String eventKey;
    
    @Column(name = "EVENT_VALUE", columnDefinition = "VARCHAR(160)")
    private String eventValue;
    
    @Column(name = "GROUP_ID", columnDefinition = "VARCHAR(160)")
    private String groupId;
    
    @Column(name = "SMS_SENT", columnDefinition = "VARCHAR(1)")
    private String smsSent;
  
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID", columnDefinition = "INTEGER")
    private Event event;
    
    
    //Getters & Setters
    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
    
    
    //ToString
    @Override
    public String toString() {
        return "EventProperty [propertyId=" + propertyId + ", eventKey=" + eventKey + ", eventValue=" + eventValue + ", groupId=" + groupId
                + ", smsSent=" + smsSent + "]";
    } 
    
}