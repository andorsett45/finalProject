package com.oreillyauto.domain;

import java.util.Arrays;

public class SmsData {
    
    //Variables
    private String phoneNumber;
    private Person [] events;
    
    //Constructors
    public SmsData() {
        super();
    }
    public SmsData(String phoneNumber, Person[] events) {
        super();
        this.phoneNumber = phoneNumber;
        this.events = events;
    }
    
    //Getters & Setters
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Person[] getEvents() {
        return events;
    }
    public void setEvents(Person[] events) {
        this.events = events;
    }
    
    //ToString
    @Override
    public String toString() {
        return "SmsData [phoneNumber=" + phoneNumber + ", events=" + Arrays.toString(events) + "]";
    }
    
}
