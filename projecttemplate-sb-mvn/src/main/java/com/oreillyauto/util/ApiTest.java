package com.oreillyauto.util;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.oreillyauto.domain.ChuckNorris;


@Controller
public class ApiTest {
    
    @ResponseBody
    @GetMapping(value = { "/rest/testFinalProjectRestv1" })
    public ChuckNorris getTestAuthenticatedRestServiceV1(HttpServletRequest req) {
        // Logic in the controller! Argh! Training Purposes Only!

        // Define the server uri, context path, and endpoint
        String url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random";

        // Setup authentication and encode it
        String auth = "7db201b928mshd5d2ab70e9948b6p1f891cjsn959f0edea9dc";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));

        // Create Request Headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.put("x-rapidapi-host", Arrays.asList("matchilling-chuck-norris-jokes-v1.p.rapidapi.com"));
        headers.put("x-rapidapi-key", Arrays.asList("7db201b928mshd5d2ab70e9948b6p1f891cjsn959f0edea9dc"));
        headers.put("accept", Arrays.asList("application/json"));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.set("Authorization", "Basic " + new String(encodedAuth));

        // Create Request Body (Payload) (if applicable)
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

        // Send The Request to the Web Service and Print the Response
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<String> response = restTemplate.getForEntity(url, entity, String.class);
        ResponseEntity<ChuckNorris> response = restTemplate.exchange(url, HttpMethod.GET, entity, ChuckNorris.class);
                                                              
        System.out.println(response.getBody());
        return response.getBody(); 
        //return "done";
    }


}