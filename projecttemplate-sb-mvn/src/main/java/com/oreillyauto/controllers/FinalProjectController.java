package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.OrlyTable;
import com.oreillyauto.domain.Response;
import com.oreillyauto.domain.SmsData;
import com.oreillyauto.service.FinalProjectService;

@Controller
public class FinalProjectController {

    @Autowired
    FinalProjectService finalProjectService;

    @GetMapping(value = { "/finalproject" })
    public String getEvents(Model model) {
        List<OrlyTable> eventList = finalProjectService.getEvents();
        /*//Checking that the Repo prints to console
         * for (OrlyTable event : eventList) {
            System.out.println(event);
        }*/
        model.addAttribute("eventListJson", eventList);
        return "finalproject";
    }

    //Gets all the events and puts them in the Orly table
    @ResponseBody
    @GetMapping(value = { "/finalproject/getFinalProjectData" })
    public List<OrlyTable> getEventsData(Model model) {

        List<OrlyTable> eventList = finalProjectService.getEvents();
        return eventList;
    }

    //Adds an Event to the Orly Table & saves it to the databse
    @ResponseBody
    @GetMapping(value = { "/finalproject/addEvent" })
    public Response getAddEvent() {
        Response response = new Response();

        boolean saved = finalProjectService.saveEvent();
        response.setEventList(finalProjectService.getEvents());
        if (saved) {
            response.setMessageType("success"); //should i change it to save.getEventId() ???
            response.setMessage("Transaction Id: Saved Successfully!");
        } else {
            response.setMessageType("danger");
            response.setMessage("Transaction Id: Not Saved Successfully!");
        }
        return response;
    }

    //Method for sending SMS messages using Twilio; returns a Response
    @ResponseBody
    @PostMapping(value = { "/finalproject/sendMessage" })
    public Response sendMessage(@RequestBody SmsData data) throws InterruptedException {
        Response response = new Response();
        try {
            String strResponse = finalProjectService.sendSMS(data);

            //Validation checking if number is verified or not
            if (strResponse.contains("landline")) {
                
                response.setMessageType("danger");
                response.setMessage(strResponse);

            } else if (strResponse.contains("voip")) {
                
                response.setMessageType("danger");
                response.setMessage(strResponse);
                
            } else if (strResponse.contains("is unverified")) {
                response.setMessageType("danger");
                response.setMessage("Sorry, this number isn't verified.");
                
            } else {
                
                response.setMessageType("success");
                response.setMessage(strResponse);
            }

        }
        catch (Exception e) {

            response.setMessageType("danger");
            response.setMessage("An error has occured. SMS message(s) could not be sent if error persists please contact developers.");

        }
        return response;
    }

}
