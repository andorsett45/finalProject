package com.oreillyauto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.FinalProjectRepositoryCustom;
import com.oreillyauto.domain.Event;
import com.oreillyauto.domain.EventProperty;
import com.oreillyauto.domain.QEventProperty;

@Repository
public class FinalProjectRepositoryImpl extends QuerydslRepositorySupport implements FinalProjectRepositoryCustom {
    
    QEventProperty eventPropertyTable = QEventProperty.eventProperty;
        
    public FinalProjectRepositoryImpl() {
        super(Event.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Event> getEvents() {
        String sql = "SELECT * " + 
                     "  FROM events ";
        Query query = getEntityManager().createNativeQuery(sql);
        return (List<Event>) query.getResultList();

    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<EventProperty> getByGroupIds(List<String> groupList) { 
              return (List<EventProperty>) (Object) 
                      from(eventPropertyTable)
                      .where(eventPropertyTable.groupId.in(groupList))
                      .fetch();
                      
    
    }

}

