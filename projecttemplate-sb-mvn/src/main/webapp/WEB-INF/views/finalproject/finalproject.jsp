<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Chuck Norris Joke Service</h1>

<div class="card">
	<div class="card-header">
		<p>The Chuck Norris Joke Service allows a user to click the "Random Joke"
			button to get a random Chuck Norris joke! The Joke Service also allows a user to
			click a toggle, enter a cell number, and click send to receive an SMS
			message(s) with the random joke(s) you have selected.</p>
		<p>WARNING/DISCLAIMER: The developer realized to late that there are NSFW Chuck Norris jokes within this API. 
			The developer has created a filter for some of the bad words but doesn't know if they found them all.
			Some jokes have been chosen as SFW with the viewing database you are currently seeing. If you click 
			the "Random Joke" button there is a chance a NSFW joke may show up, user discretion is advised.</p>
	</div>
	<div class="card-body">
		<div class='row'>
			<c:set value="${fn:length(eventListJson) gt 0 ? eventListJson : []}"
				var="tableData" />
			<div class="col-sm-12">
				<form>
					<c:if test="${not empty event}">
						<input type="hidden" id="update" name="update" value="true" />
					</c:if>

					<div class='row form-group'>
						<div class="col-sm-4 form-group">
							<!-- Send Cell Number Info -->
							<!-- <input id='cellNumber' type="tel" pattern="[+1]{2}[0-9]{3}[0-9]{3}[0-9]{4}" required placeholder='Cell Number'/> -->
							<input id='cellNumber' type="tel" placeholder='Cell Number' />
							<button id="sendBtn" spinonclick type='submit' class='btn btn-primary'>Send</button>
						</div>
						<div class="col-sm-4 form-group">
							<!-- Choose Joke Id? -->
							<!-- <input id='jokeId' type="text" placeholder='Joke Id'/> -->
							<orly-button id="submitBtn" spinonclick class='btn btn-success'>Random Joke</orly-button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<c:set value="${fn:length(eventListJson) gt 0 ? eventListJson : []}" var="tableData" />
	<orly-table id="eventTable" tabletitle="Search" includefilter loaddataoncreate url='<c:url value="/finalproject/getFinalProjectData"/>' > 
		<orly-column class="col-md-1 col-3" name="smsSent" field="action" label="Action" sorttype="natural">
			<div slot="cell">
				<orly-toggle name="toggle[]" class="jokeToggle" type="checkbox" data-joke ="\${model.joke}" id="\${`\${model.id}`}" checked="\${model.smsSent == '1' ? true : false}"></orly-toggle>
			</div>
		</orly-column>
		<orly-column class="col-md-1 col-3" field="id" label="ID"></orly-column>
		<orly-column class="col-md-10 col-9" name="joke" id="\${`joke_\${model.id}`}" field="joke" label="Joke"></orly-column>  
		
	</orly-table>

<script>

var debug = 1;
var jsonString = "";
var jsonUserArray;
var eventTable;
var submitBtn;
var sendBtn;
var contextPath = '${pageContext.request.contextPath}';
var checkedArray = [];
var smsArray = [];


window.addEventListener('DOMContentLoaded', (event) => {
	
	// Initialize the table element variable
	eventTable = orly.qid("eventTable");
	submitBtn = orly.qid("submitBtn");
	sendBtn = orly.qid("sendBtn");
	
	submitBtn.addEventListener("click", function(e) {
		try {
			e.preventDefault();
				
				// AJAX call to delete id
				fetch("<c:url value='/finalproject/addEvent' />", {
			        method: "GET",
			        /* body: JSON.stringify(User), */
			        headers: {
			            "Content-Type": "application/json"
			        }
				}).then(function(response) {
				  	if (response.ok) {
				  		let jsonPromise = response.json(); // object from controller
				  		return jsonPromise;
				  	} else {
				  		throw new Error(getError(response));
				  	}
				}).then(function(Response) {						
					if (Response != null && Response != "undefined") {
						let messageType = "info";
						let message = Response.message;
						messageType = ((Response.messageType.length > 0) ? Response.messageType : messageType); 
						orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
						
						populateTableWithResponse(Response);
					} else {
						orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
					}
				}).catch(function(error) {
					console.log('There was a problem with your fetch operation: ', error.message);
				});
			
		} catch (err) {
			console.log("Runtime Error-> " + err);
		} 
	});
	
	sendBtn.addEventListener("click", function(e){
		e.preventDefault(); // Prevent Form From Submitting "normally" (IE)
		
		let cellNum = orly.qid("cellNumber").value;
		//let regex = /[+1]{2}[0-9]{3}[0-9]{3}[0-9]{4}/g;
		let regex = /[0-9]{3}[0-9]{3}[0-9]{4}/g;
		
		//Checking if phone number is in input box; if not throw error
		if (cellNum == null || cellNum == ""){
			let message = "Need to enter cell phone number!";
			orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
			return;
		}
		//Checking if phone number is in correct format; if not throw error
		else if (!regex.test(cellNum)){
			let message = "Enter cell number in correct format! Format: +1#########";
			orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
			return;
		}
		//Check if toggles are toggled, if they are put them in array (for error checking) and disable them
		else {
			
			let checkedToggleList = orly.qa(".jokeToggle");	
			let jokeList = [];	
			
			for (let toggle of checkedToggleList) {
				
				if (toggle.checked) {
					
					let groupId = toggle.id; 
					
					let toggleDisabled = toggle.getAttribute("disabled");
					
					if (toggleDisabled != "true") {
						
						smsArray.push(groupId);	
						let jokeGroupId = eventTable.getItem(toggle).id;
						let joke = eventTable.getItem(toggle).joke;
						
						let Person = {};
						Person.id = jokeGroupId;
						Person.message = joke;
						jokeList.push(Person);
					}
				}
			}	
						
				// Check if the toggles are toggled; if nothing in list then nothing is toggled --> throw error
				if (smsArray.length == 0) {
					let message = "You have to select a row in order to send it!";
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
					return;
				}
				
				//Create Person Object to send to JSON
				let SmsData = {};
				SmsData.phoneNumber = orly.qid("cellNumber").value;
				SmsData.events = jokeList;
				
				
				fetch("<c:url value='/finalproject/sendMessage' />", {
			        method: "POST",
			        body: JSON.stringify(SmsData),
			        headers: {
			            "Content-Type": "application/json"
			        }
				}).then(function(response) {
				  	if (response.ok) {
				  		console.log("response.status=", response.status);
				  		let message = response.json();
				  		console.log("message = " + message);
				  		return message
				  	} else {
				  		throw new Error("Error: " + response.statusText);
				  	}
				}).then(function(Response) {
					//let Message = JSON.parse(response);
					let message = Response.message;
					let messageType = Response.messageType;
					
					if (messageType == null || messageType == "undefined" || messageType.length == 0) {
						messageType = "info";
					}
				  	
				  	if (message != null && message != "undefined" && message.length > 0) {
				  		orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});	
				  	}
				  	
				  	//Set toggle as Disabled and Checked
				  	for (let person of jokeList){
				  		disableToggle(person.id);
				  	}
				  	
				}).catch(function(error) {
					let message = 'There was a problem with your fetch operation: ' + error.message;
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
				});
						
		}
});		
	
});

function disableToggle(id) {
    let toggle = orly.qid(id);
    
    if (toggle) {
        toggle.setAttribute("checked", "true");
        toggle.setAttribute("disabled", "true");    
    }
}

function populateTableWithResponse(Response) {
	try {
		let eventList = Response.eventList;
		
		if (eventList != null && eventList != "undefined") {
			eventTable.data = eventList;
			eventTable.loadData();
			//eventTable.data = [data];
		} else {
			console.log("Unable to populate table with new event list");
		}	
	} catch (e) {
		console.log("Unable to populate table with new event list");	
	}
}

function getError(errorObject) {
	if (errorObject == null || errorObject == "undefined") {
		return "Unknown Exception";
	} else {
		let status = errorObject.status;
		let text = errorObject.statusText;
		console.log(status + " " + text);
		return "Server Error. Status="+status+" Msg="+text;
	}
}


</script>